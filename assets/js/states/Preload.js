var Match3 = Match3 || {};

Match3.PreloadState = {
	preload: function(){
		this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bar');
		this.preloadBar.anchor.setTo(0.5);	
		this.preloadBar.scale.setTo(0.5);	
		this.load.setPreloadSprite(this.preloadBar);

		this.load.image('block1', 'assets/img/plastico.png');
		this.load.image('block2', 'assets/img/metal.png');
		this.load.image('block3', 'assets/img/vidro.png');
		this.load.image('block4', 'assets/img/organico.png');
		this.load.image('block5', 'assets/img/papel.png');
		this.load.image('block6', 'assets/img/bean_yellow.png');
		this.load.image('block7', 'assets/img/bean_red.png');
		this.load.image('deadBlock', 'assets/img/dead-block.png');
		this.load.image('background', 'assets/img/background.jpg');
		this.load.audio('score', 'assets/som/score.mp3');
		this.load.audio('musica', 'assets/som/musica.mp3');
	},

	create: function(){
		this.state.start('Game');
	}
};